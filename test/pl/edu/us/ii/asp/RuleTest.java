package pl.edu.us.ii.asp;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import static pl.edu.us.ii.asp.DLP.*;

public class RuleTest  {
	
	@Test
	public void testToString() {
		Rule rule = new Rule(
			Atoms(Atom("A")),
			Atoms(Atom("B")),
			Atoms(Atom("C")),
			Conds(Cond("(true)", (String ...args) -> true))
		);
		assertEquals("A() :- B() and not C(). If (true)()", rule.toString());
	}
	
}
