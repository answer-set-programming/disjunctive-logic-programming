package pl.edu.us.ii.asp;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class AtomTest  {

	@Test
	public void testGetPredicate() {
		Atom atom = new Atom("sampleatom");
		assertEquals("sampleatom", atom.getPredicate());
	}
	
	@Test
	public void testGetTerms() {
		Atom atom = new Atom("", "a", "A");
		String[] expectedTerms = {"a", "A"};
		assertArrayEquals(expectedTerms, atom.getTerms());
	}
	
	@ParameterizedTest
	@CsvSource({ "a, A, 1", "A, A, 0", "a, a, 2" })
	public void testGetNumberOfConstants(String firstTerm, String secondTerm, int expected) {
		Atom atom = new Atom("", firstTerm, secondTerm);
		assertEquals(expected, atom.getNumberOfConstants());
	}
	
	@ParameterizedTest
	@CsvSource({ "a, A, 1", "A, A, 2", "a, a, 0" })
	public void testGetNumberOfVariables(String firstTerm, String secondTerm, int expected) {
		Atom atom = new Atom("", firstTerm, secondTerm);
		assertEquals(expected, atom.getNumberOfVariables());
	}
	
	@Test
	public void testToString() {
		Atom atom = new Atom("sampleatom", "a", "A");
		assertEquals("sampleatom(a, A)", atom.toString());
	}
	
	@Test
	public void testHashCode() {
		Atom atom = new Atom("sampleatom", "a", "A");
		assertEquals("sampleatom(a, A)".hashCode(), atom.hashCode());
	}
	
}
