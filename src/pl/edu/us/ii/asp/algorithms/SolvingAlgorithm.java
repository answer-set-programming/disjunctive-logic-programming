package pl.edu.us.ii.asp.algorithms;

import java.util.Set;
import pl.edu.us.ii.asp.Atom;
import pl.edu.us.ii.asp.Program;

public interface SolvingAlgorithm {

	/**
	 * Solve DLP program. Returns a stable model as a set of atoms;
	 * @param program     DLP program
	 * @param timeLimit   Limit for execution in seconds
	 * @param memoryLimit Limit for memory (could be bytes or the number of created nodes etc.)
	 * @return
	 */
	public Set<Atom> solve(Program program, int timeLimit, int memoryLimit);
	
}
