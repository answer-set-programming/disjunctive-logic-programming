package pl.edu.us.ii.asp;

/**
 * An interface for a function (a class or lambda expression) in conditions.
 */
@FunctionalInterface
public interface ConditionFunction {
	
	public boolean apply(String ...args);
	
}