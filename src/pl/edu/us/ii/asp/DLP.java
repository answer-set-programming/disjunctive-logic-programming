package pl.edu.us.ii.asp;

/**
 * A helper that makes the API simpler.
 */
public class DLP {
	
	/**
	 * Check if the provided term is a variable (start with an uppercase letter).
	 * @param term A term as a string.
	 */
	public static boolean isVariable(String term) {
		return Character.isUpperCase(term.charAt(0));
	}
	
	/**
	 * Check if the provided term is a constant.
	 * @param term A term as a string.
	 */
	public static boolean isConstant(String term) {
		return Character.isUpperCase(term.charAt(0)) == false;
	}

	/**
	 * A alias for creating an atom.
	 */
	public static Atom Atom(String predicate, String ...terms) {
		return new Atom(predicate, terms);
	}
	
	/**
	 * An alias for creating an array of atoms.
	 */
	public static Atom[] Atoms(Atom ...atoms) {
		return atoms;
	}
	
	/**
	 * An alias for creating a rule.
	 */
	public static Rule Rule(Atom[] head, Atom[] positiveAtoms, Atom[] negativeAtoms, Condition[] conditions) {
		return new Rule(head, positiveAtoms, negativeAtoms, conditions);
	}
	
	/**
	 * An alias for creating a rule.
	 */
	public static Rule Rule(Atom[] head, Atom[] positiveAtoms, Atom[] negativeAtoms) {
		return new Rule(head, positiveAtoms, negativeAtoms);
	}
	
	/**
	 * An alias for creating a rule.
	 */
	public static Rule Rule(Atom[] head, Atom[] positiveAtoms) {
		return new Rule(head, positiveAtoms);
	}
	
	/**
	 * An alias for creating a rule.
	 */
	public static Rule Rule(Atom[] head) {
		return new Rule(head);
	}
	
	/**
	 * An alias for creating a rule.
	 */
	public static Rule Rule(Atom headAtom, Atom[] positiveAtoms, Atom[] negativeAtoms, Condition[] conditions) {
		return new Rule(headAtom, positiveAtoms, negativeAtoms, conditions);
	}
	
	/**
	 * An alias for creating a rule.
	 */
	public static Rule Rule(Atom headAtom, Atom[] positiveAtoms, Atom[] negativeAtoms) {
		return new Rule(headAtom, positiveAtoms, negativeAtoms);
	}
	
	/**
	 * An alias for creating a rule.
	 */
	public static Rule Rule(Atom headAtom, Atom[] positiveAtoms) {
		return new Rule(headAtom, positiveAtoms);
	}
	
	/**
	 * An alias for creating a rule.
	 */
	public static Rule Rule(Atom headAtom) {
		return new Rule(headAtom);
	}
	
	/**
	 * An alias for creating an array of rules.
	 */
	public static Rule[] Rules(Rule ...rules) {
		return rules;
	}
	
	/**
	 * An alias for creating a condition.
	 */
	public static Condition Condition(String name, ConditionFunction predicate, String ...terms) {
		return new Condition(name, predicate, terms);
	}
	
	/**
	 * An alias for creating a condition.
	 */
	public static Condition Condition(ConditionFunction predicate, String ...terms) {
		return new Condition(predicate, terms);
	}
	
	/**
	 * An array for creating an array of conditions.
	 */
	public static Condition[] Conditions(Condition ...conditions) {
		return conditions;
	}
	
	/**
	 * An alias for creating a condition.
	 */
	public static Condition Cond(String name, ConditionFunction predicate, String ...terms) {
		return new Condition(name, predicate, terms);
	}
	
	/**
	 * An alias for creating a condition.
	 */
	public static Condition Cond(ConditionFunction predicate, String ...terms) {
		return new Condition(predicate, terms);
	}
	
	/**
	 * An array for creating an array of conditions.
	 */
	public static Condition[] Conds(Condition ...conditions) {
		return conditions;
	}
	
}
