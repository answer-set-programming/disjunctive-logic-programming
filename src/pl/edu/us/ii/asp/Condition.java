package pl.edu.us.ii.asp;

/**
 * A condition is similar to an atom (see {@link pl.edu.us.ii.asp.Atom} class) except a condition function.
 */
public class Condition extends Atom {
	
	private static int id = 1;

	private ConditionFunction function;
	
	/**
	 * Creates a condition.
	 * @param name		A name of a condition
	 * @param function	A function that validate a condition
	 * @param terms		An array of strings
	 */
	public Condition(String name, ConditionFunction function, String ...terms) {
		super(name, terms);
		this.function = function;
	}
	
	/**
	 * Creates an auto-named condition (i.e. a condition will get a name in the form: condition#{id}).
	 * @param function	A function that validate a condition
	 * @param terms		An array of strings
	 */
	public Condition(ConditionFunction predicate, String ...terms) {
		this("condition#" + (id++), predicate, terms);
	}
	
	/**
	 * Return a result of a condition function execution with terms as arguments.
	 * @return A result that should be true or false.
	 */
	public boolean test() {
		return function.apply(terms);
	}
	
}
