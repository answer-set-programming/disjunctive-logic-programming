package pl.edu.us.ii.asp;

import static pl.edu.us.ii.asp.DLP.*;

/**
 * An atom is <code>a(t<sub>1</sub>,...,t<sub>n</sub>)</code>, where <code>a</code> is a predicate and <code>t<sub>1</sub>,...,t<sub>n</sub></code> are terms.
 */
public class Atom {
	
	private String predicate;
	protected String[] terms;
	private int hashCode;
	private int nofConstants;
	private int nofVariables;
	
	/**
	 * Returns a predicate of an atom.
	 * @return A predicate as a string.
	 */
	public String getPredicate() {
		return predicate;
	}

	/**
	 * Returns terms of an atom.
	 * @return An array of terms.
	 */
	public String[] getTerms() {
		return terms;
	}
	
	/**
	 * Returns the number of constants in terms.
	 */
	public int getNumberOfConstants() {
		return nofConstants;
	}
	
	/**
	 * Returns the number of variables in terms.
	 */
	public int getNumberOfVariables() {
		return nofVariables;
	}

	/**
	 * Creates an atom.
	 * @param predicate	A predicate as a string.
	 * @param terms		Terms as an array of strings. 
	 */
	public Atom(String predicate, String ...terms) {
		this.predicate = predicate;
		this.terms = terms;
		for (String term: terms) {
			if (isVariable(term)) {
				nofVariables++;
			} else {
				nofConstants++;
			}
		}
		this.hashCode = this.toString().hashCode();
	}
	
	@Override
	public String toString() {
		return predicate + "(" + String.join(", ", terms) + ")";
	}
	
	@Override
	public int hashCode() {
		return hashCode;
	}

}
 