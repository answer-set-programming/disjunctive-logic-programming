package pl.edu.us.ii.asp;

import static pl.edu.us.ii.asp.DLP.*;

/**
 * A (disjunctive) rule <code>r</code> is a clause of the form:<br>
 * <code>a<sub>1</sub>&or;...&or;a<sub>n</sub> &larr; b<sub>1</sub>&and;...&and;b<sub>k</sub>&and;&not;b<sub>k+1</sub>&and;...&and;&not;b<sub>m</sub> if c<sub>1</sub>&and;...&and;c<sub>o</sub> n&ge;1, m&ge;0, o&ge;0</code><br>
 * where <code>a<sub>1</sub>&or;...&or;a<sub>n</sub>, b<sub>1</sub>&or;...&or;b<sub>m</sub></code> are atoms and <code>c<sub>1</sub>&and;...&and;c<sub>o</sub></code> are conditions.
 * The body atoms are called literals, which are either positive or negative.
 */
public class Rule {

	private static Atom[] emptyAtoms = {};
	private static Condition[] emptyConditions = {};
	
	private Atom[] head;
	private Atom[] positiveAtoms; // body
	private Atom[] negativeAtoms; // body
	private Condition[] conditions; // conditions
	private int hashCode;
	
	/**
	 * Return the head's atoms of <code>r</code> (i.e. <code>H(r)</code>).
	 * @return An array of atoms object 
	 */
	public Atom[] getHead() {
		return head;
	}

	/**
	 * Return the positive literals of <code>r</code>'s body (i.e. <code>B<sup>+</sup>(r)</code>).
	 * @return An array of atoms object 
	 */
	public Atom[] getPositiveAtoms() {
		return positiveAtoms;
	}

	/**
	 * Return the negative literals of <code>r</code>'s body (i.e. <code>B<sup>-</sup>(r)</code>).
	 * @return An array of atoms object 
	 */
	public Atom[] getNegativeAtoms() {
		return negativeAtoms;
	}

	/**
	 * Return the conditions of <code>r</code> (i.e. <code>C(r)</code>).
	 * @return An array of conditions object 
	 */
	public Condition[] getConditions() {
		return conditions;
	}

	/**
	 * Creates a rule <code>r</code>.
	 * @param head			An array of atoms (i.e. <code>H(r)</code>)
	 * @param positiveAtoms	An array of atoms (i.e. <code>B<sup>+</sup>(r)</code>)
	 * @param negativeAtoms	An array of atoms (i.e. <code>B<sup>-</sup>(r)</code>)
	 * @param conditions	An array of conditions (i.e. <code>C(r)</code>)
	 */
	public Rule(Atom[] head, Atom[] positiveAtoms, Atom[] negativeAtoms, Condition[] conditions) {
		this.head = head != null ? head : emptyAtoms;
		this.positiveAtoms = positiveAtoms != null ? positiveAtoms : emptyAtoms;
		this.negativeAtoms = negativeAtoms != null ? negativeAtoms : emptyAtoms;
		this.conditions = conditions != null ? conditions : emptyConditions;
		this.hashCode = this.toString().hashCode();
	}
	
	/**
	 * Creates a rule <code>r</code> with the head and all literals only.
	 * @param head			An array of atoms (i.e. <code>H(r)</code>)
	 * @param positiveAtoms	An array of atoms (i.e. <code>B<sup>+</sup>(r)</code>)
	 * @param negativeAtoms	An array of atoms (i.e. <code>B<sup>-</sup>(r)</code>)
	 */
	public Rule(Atom[] head, Atom[] positiveAtoms, Atom[] negativeAtoms) {
		this(head, positiveAtoms, negativeAtoms, emptyConditions);
	}
	
	/**
	 * Creates a rule <code>r</code> with the head and positive literals only.
	 * @param head			An array of atoms (i.e. <code>H(r)</code>)
	 * @param positiveAtoms	An array of atoms (i.e. <code>B<sup>+</sup>(r)</code>)
	 */
	public Rule(Atom[] head, Atom[] positiveAtoms) {
		this(head, positiveAtoms, emptyAtoms, emptyConditions);
	}
	
	/**
	 * Creates a rule <code>r</code> with the head only.
	 * @param head			An array of atoms (i.e. <code>H(r)</code>)
	 */
	public Rule(Atom[] head) {
		this(head, emptyAtoms, emptyAtoms, emptyConditions);
	}
	
	/**
	 * Creates a rule <code>r</code>.
	 * @param headAtom			A single atom (i.e. <code>H(r)</code>)
	 * @param positiveAtoms	An array of atoms (i.e. <code>B<sup>+</sup>(r)</code>)
	 * @param negativeAtoms	An array of atoms (i.e. <code>B<sup>-</sup>(r)</code>)
	 * @param conditions	An array of conditions (i.e. <code>C(r)</code>)
	 */
	public Rule(Atom headAtom, Atom[] positiveAtoms, Atom[] negativeAtoms, Condition[] conditions) {
		this.head = headAtom != null ? Atoms(headAtom) : emptyAtoms;
		this.positiveAtoms = positiveAtoms != null ? positiveAtoms : emptyAtoms;
		this.negativeAtoms = negativeAtoms != null ? negativeAtoms : emptyAtoms;
		this.conditions = conditions != null ? conditions : emptyConditions;
		this.hashCode = this.toString().hashCode();
	}
	
	/**
	 * Creates a rule <code>r</code> with the head and all literals only.
	 * @param headAtom			A single atom (i.e. <code>H(r)</code>)
	 * @param positiveAtoms	An array of atoms (i.e. <code>B<sup>+</sup>(r)</code>)
	 * @param negativeAtoms	An array of atoms (i.e. <code>B<sup>-</sup>(r)</code>)
	 */
	public Rule(Atom headAtom, Atom[] positiveAtoms, Atom[] negativeAtoms) {
		this(Atoms(headAtom), positiveAtoms, negativeAtoms, emptyConditions);
	}
	
	/**
	 * Creates a rule <code>r</code> with the head and positive literals only.
	 * @param headAtom			A single atom (i.e. <code>H(r)</code>)
	 * @param positiveAtoms	An array of atoms (i.e. <code>B<sup>+</sup>(r)</code>)
	 */
	public Rule(Atom headAtom, Atom[] positiveAtoms) {
		this(Atoms(headAtom), positiveAtoms, emptyAtoms, emptyConditions);
	}
	
	/**
	 * Creates a rule <code>r</code> with the head only.
	 * @param headAtom			An array of atoms (i.e. <code>H(r)</code>)
	 */
	public Rule(Atom headAtom) {
		this(Atoms(headAtom), emptyAtoms, emptyAtoms, emptyConditions);
	}
	
	@Override
	public String toString() {
		String[] head = new String[this.head.length];
		String[] positiveAtoms = new String[this.positiveAtoms.length];
		String[] negativeAtoms = new String[this.negativeAtoms.length];
		String[] conditions = new String[this.conditions.length];
		for (int i = 0; i < this.head.length; ++i) head[i] = this.head[i].toString();
		for (int i = 0; i < this.positiveAtoms.length; ++i) positiveAtoms[i] = this.positiveAtoms[i].toString(); 
		for (int i = 0; i < this.negativeAtoms.length; ++i) negativeAtoms[i] = "not " + this.negativeAtoms[i].toString(); 
		for (int i = 0; i < this.conditions.length; ++i) conditions[i] = this.conditions[i].toString(); 
		return String.join(" or ", head) + " :- " + String.join(" and ", positiveAtoms)
			+ (negativeAtoms.length > 0 && positiveAtoms.length > 0 ? " and " : "")
			+ String.join(" and ", negativeAtoms)
			+ "."
			+ (conditions.length > 0  ? " If " : "")
			+ String.join(" and ", conditions);
	}
	
	@Override
	public int hashCode() {
		return hashCode;
	}

}
