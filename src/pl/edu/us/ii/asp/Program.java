package pl.edu.us.ii.asp;

import java.util.HashSet;
import java.util.Set;

import static pl.edu.us.ii.asp.DLP.*;

/**
 * A program is a set of rules. See {@link pl.edu.us.ii.asp.Rule} class to read about a rule construction.
 * The class provides additional rules creating methods to simplify API.  
 */
public class Program {
	
	private final static String constraintAtomPredicate = "__aux";

	private Set<Rule> rules = new HashSet<Rule>();
	
	/**
	 * Return a set of rules as an array.
	 * @return
	 */
	public Rule[] getRulesAsArray() {
		int n = rules.size(); 
        Rule arr[] = new Rule[n];
        System.arraycopy(rules.toArray(), 0, arr, 0, n);
		return arr;
	}
	
	/**
	 * Return a set of rules.
	 * @return
	 */
	public Set<Rule> getRules() {
		return rules;
	}

	/**
	 * Add a single rule.
	 * @param rule A rule that will be added to a program
	 */
	public void addRule(Rule rule) {
		rules.add(rule);
	}
	
	/**
	 * Add several rules to a program.
	 * @param rules A set of rules
	 */
	public void addRules(Rule ...rules) {
		for (Rule rule: rules) this.rules.add(rule);
	}
	
	/**
	 * Add a rule with the empty body (i.e. fact) that includes a single atom in the head.
	 * @param predicate	A predicate for the atom
	 * @param terms		A set of terms for the atom
	 */
	public void addFact(String predicate, String ...terms) {
		addRule(Rule(Atoms(Atom(predicate, terms))));
	}
	
	/**
	 * Add several facts (a rule with the empty body) that consist the head with a single atom, where each atom has only one term and the same predicate.<br>
	 * The exemplary code <code>addFacts("person", "John", "Paul", "Michael")</code> produce the following rules:<br>
	 * - <code>person(John) :- .</code><br>
	 * - <code>person(Paul) :- .</code><br>
	 * - <code>person(Michael) :- .</code>
	 * @param predicate	A predicate for all atoms
	 * @param terms		Each provided term will create a new fact that includes a single atom
	 */
	public void addFacts(String predicate, String ...terms) {
		for (String term: terms) {
			addRule(Rule(Atoms(Atom(predicate, term))));
		}
	}
	
	/**
	 * Add a rule with the empty head (i.e. constraint).
	 * @param positiveAtoms	A set of atoms, could be <code>null</code>
	 * @param negativeAtoms	A set of negated atoms, could be <code>null</code>
	 * @param conditions	A set of conditions, could be <code>null</code>
	 */
	public void addConstraint(Atom[] positiveAtoms, Atom[] negativeAtoms, Condition[] conditions) {
		negativeAtoms = negativeAtoms != null ? negativeAtoms : Atoms(); // validate input
		Atom[] mergedNegativeAtoms = new Atom[negativeAtoms.length + 1];
		System.arraycopy(negativeAtoms, 0, mergedNegativeAtoms, 0, negativeAtoms.length);  
		System.arraycopy(Atoms(Atom(constraintAtomPredicate)), 0, mergedNegativeAtoms, negativeAtoms.length, 1);
		addRule(Rule(Atom(constraintAtomPredicate), positiveAtoms, mergedNegativeAtoms, conditions));
	}
	
	/**
	 * Add a rule with the empty head (i.e. constraint).
	 * @param positiveAtoms	A set of atoms, could be <code>null</code>
	 * @param negativeAtoms	A set of negated atoms, could be <code>null</code>
	 */
	public void addConstraint(Atom[] positiveAtoms, Atom[] negativeAtoms) {
		negativeAtoms = negativeAtoms != null ? negativeAtoms : Atoms(); // validate input
		Atom[] mergedNegativeAtoms = new Atom[negativeAtoms.length + 1];
		System.arraycopy(negativeAtoms, 0, mergedNegativeAtoms, 0, negativeAtoms.length);  
		System.arraycopy(Atoms(Atom(constraintAtomPredicate)), 0, mergedNegativeAtoms, negativeAtoms.length, 1); 
		addRule(Rule(Atom(constraintAtomPredicate), positiveAtoms, mergedNegativeAtoms));
	}
	
	/**
	 * Add a rule with the empty head (i.e. constraint).
	 * @param positiveAtoms	A set of atoms, could be <code>null</code>
	 */
	public void addConstraint(Atom[] positiveAtoms) {
		addRule(Rule(Atom(constraintAtomPredicate), positiveAtoms, Atoms(Atom(constraintAtomPredicate))));
	}

}
